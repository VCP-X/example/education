/*
 *  Copyright (c) 2022-2023 QuantumSurf Co., Ltd. All rights reserved.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Apache License, Version 2.0 which is available at
 *  https://www.apache.org/licenses/LICENSE-2.0
 *
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Contributors:
 *       QuantumSurf Co., Ltd. - initial API and implementation
 *
 */

plugins {
    id("java")
}

tasks.withType<Jar> {
    manifest {
        attributes["Main-Class"] = "com.vcp.education.BackendService"
    }
}