/*
 *  Copyright (c) 2022-2023 QuantumSurf Co., Ltd. All rights reserved.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Apache License, Version 2.0 which is available at
 *  https://www.apache.org/licenses/LICENSE-2.0
 *
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Contributors:
 *       QuantumSurf Co., Ltd. - initial API and implementation
 *
 */

plugins {
    `java-library`
    id("application")
    id("com.github.johnrengelman.shadow") version "7.1.2"
}

repositories {
    mavenCentral()
    maven {
        url = uri("https://maven.iais.fraunhofer.de/artifactory/eis-ids-public/")
    }
}

val edcGroupId: String by project
val edcVersion: String by project

dependencies {
    implementation("${edcGroupId}:control-plane-core:${edcVersion}")
    implementation("${edcGroupId}:ids:${edcVersion}")
    implementation("${edcGroupId}:configuration-filesystem:${edcVersion}")
    implementation("${edcGroupId}:vault-hashicorp:${edcVersion}")
    implementation("${edcGroupId}:oauth2-core:${edcVersion}")
    implementation("${edcGroupId}:oauth2-daps:${edcVersion}")
    implementation("${edcGroupId}:auth-tokenbased:${edcVersion}")
    implementation("${edcGroupId}:management-api:${edcVersion}")
    implementation("${edcGroupId}:catalog-api:${edcVersion}")
    implementation("${edcGroupId}:transfer-pull-http-receiver:${edcVersion}")
    implementation("${edcGroupId}:data-plane-selector-api:${edcVersion}")
    implementation("${edcGroupId}:data-plane-selector-core:${edcVersion}")
    implementation("${edcGroupId}:data-plane-selector-client:${edcVersion}")

    implementation("${edcGroupId}:transfer-data-plane:${edcVersion}")
    implementation("${edcGroupId}:data-plane-api:${edcVersion}")
    implementation("${edcGroupId}:data-plane-core:${edcVersion}")
    implementation("${edcGroupId}:data-plane-http:${edcVersion}")
}

application {
    mainClass.set("org.eclipse.edc.boot.system.runtime.BaseRuntime")
}

tasks.withType<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar> {
    exclude("**/pom.properties", "**/pom.xm")
    mergeServiceFiles()
    archiveFileName.set("http-pull-provider-connector.jar")
}