/*
 *  Copyright (c) 2022 QuantumSurf Co., Ltd.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Apache License, Version 2.0 which is available at
 *  https://www.apache.org/licenses/LICENSE-2.0
 *
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Contributors:
 *       QuantumSurf Co., Ltd. - initial API and implementation
 *
 */

package com.vcp.education.configuration;

import org.eclipse.edc.runtime.metamodel.annotation.Inject;
import org.eclipse.edc.spi.system.ServiceExtension;
import org.eclipse.edc.spi.system.ServiceExtensionContext;
import org.eclipse.edc.web.spi.WebService;

public class HealthEndpointExtension implements ServiceExtension {
    private static final String LOG_PREFIX_SETTING = "config.log.prefix";

    @Inject
    WebService webService;

    @Override
    public void initialize(ServiceExtensionContext context) {
        var logPrefix = context.getSetting(LOG_PREFIX_SETTING, "prefix");
        webService.registerResource(new HealthApiController(context.getMonitor(), logPrefix));
    }
}
