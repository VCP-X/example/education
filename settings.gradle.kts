/*
 *  Copyright (c) 2022 Fraunhofer Institute for Software and Systems Engineering
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Apache License, Version 2.0 which is available at
 *  https://www.apache.org/licenses/LICENSE-2.0
 *
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Contributors:
 *       Fraunhofer Institute for Software and Systems Engineering - initial API and implementation
 *
 */

rootProject.name = "Education"

// this is needed to have access to snapshot builds of plugins
pluginManagement {
    repositories {
        maven {
            url = uri("https://oss.sonatype.org/content/repositories/snapshots/")
        }
        maven {
            url = uri("https://aas-nest.io/content/repositories/snapshots/")
        }
        mavenCentral()
        gradlePluginPortal()
    }
}

dependencyResolutionManagement {
    repositories {
        maven {
            url = uri("https://oss.sonatype.org/content/repositories/snapshots/")
        }
        maven {
            url = uri("https://aas-nest.io/content/repositories/snapshots/")
        }
        mavenCentral()
        mavenLocal()
    }
    versionCatalogs {
        create("libs") {
            from("org.eclipse.edc:edc-versions:0.0.1-SNAPSHOT")
            // this is not part of the published EDC Version Catalog, so we'll just "amend" it
            library(
                "dnsOverHttps",
                "com.squareup.okhttp3",
                "okhttp-dnsoverhttps"
            ).versionRef("okhttp")
        }
    }
}

include(":basic-example:simple")
include(":basic-example:health")
include(":basic-example:configuration")
include(":transfer-example:http-push:http-push-provider")
include(":transfer-example:http-push:http-push-consumer")
include(":transfer-example:http-push:http-push-backend")
include(":transfer-example:http-pull:http-pull-provider")
include(":transfer-example:http-pull:http-pull-consumer")
include(":transfer-example:http-pull:http-pull-backend")
